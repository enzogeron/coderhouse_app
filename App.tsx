import 'react-native-gesture-handler'

import React from 'react'
import {StatusBar} from 'react-native'
import {Provider} from 'react-redux'
import {store} from './src/redux'
import {NavigationContainer} from '@react-navigation/native'
import {DrawerNavigator} from './src/navigator/DrawerNavigator'

export const App = () => {
  return (
    <Provider store={store}>
      <StatusBar backgroundColor="#000" />
      <NavigationContainer>
        <DrawerNavigator />
      </NavigationContainer>
    </Provider>
  )
}

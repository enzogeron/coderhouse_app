/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState} from 'react'
import {StackScreenProps} from '@react-navigation/stack'
import SplashScreen from 'react-native-splash-screen'
import {
  FlatList,
  Text,
  View,
  TouchableOpacity,
  Modal,
  Pressable,
  TextInput,
  Button,
  SafeAreaView,
  ActivityIndicator,
} from 'react-native'
import {useSelector, useDispatch} from 'react-redux'
import {RootState} from '../../redux/reducers'
import {useForm} from '../../hooks/useForm'
import {Course} from '../../interfaces'
import styles from './styles'
import {getCourses} from '../../redux/actions/courses.actions'

interface Props extends StackScreenProps<any, any> {}

const CoursesScreen = ({navigation}: Props) => {
  const dispatch = useDispatch()
  const listCourses = useSelector((state: RootState) => state.courses.courses)
  const [courses, setCourses] = useState<Course[]>(listCourses)
  const [modalVisible, setModalVisible] = useState(false)
  const {form, title, description, handleChangeText} = useForm({
    title: '',
    description: '',
  })

  function dispatchCourses() {
    dispatch(getCourses())
  }

  useEffect(() => {
    SplashScreen.hide()
  }, [])

  useEffect(() => {
    dispatchCourses()
  }, [dispatch])

  useEffect(() => {
    setCourses(listCourses)
  }, [listCourses])

  const randomId = () => {
    const upperBound = 9999
    const lowerBound = 1000
    const number = Math.floor(lowerBound + Math.random() * (upperBound - lowerBound + 1))
    return number
  }

  const handleItemFavorite = id => {
    console.log(id)
  }

  const handleItemDelete = id => {
    setCourses(courses.filter(course => course.id !== id))
  }

  const handleItemSave = () => {
    if (title.length && description.length) {
      const course: Course = {
        ...form,
        id: randomId(),
        favorite: false,
        price: 0,
        discount: 0,
        duration: '',
        type: '',
        category: '',
      }
      setCourses([course, ...courses])
      form.title = ''
      form.description = ''
      setModalVisible(false)
    }
  }

  const Favorite = ({id}) => {
    return (
      <View style={[styles.badge, styles.badgeFavorite]}>
        <Text style={styles.badgeText} onPress={() => handleItemFavorite(id)}>
          Guardar
        </Text>
      </View>
    )
  }

  const Delete = ({id}) => {
    return (
      <View style={[styles.badge]}>
        <Text style={styles.badgeTextDelete} onPress={() => handleItemDelete(id)}>
          Eliminar
        </Text>
      </View>
    )
  }

  const loadMoreCourses = () => {
    // Agregar dispatch para cargar mas cursos
  }

  const Item = ({course}) => {
    return (
      <TouchableOpacity onPress={() => navigation.navigate('DetailsCourse', {id: course.id})}>
        <View style={styles.cardContainer}>
          <Text style={styles.titleCard}>{course.title}</Text>
          <Text>{course.description}</Text>
          <View style={styles.actionsContainer}>
            <Favorite id={course.id} />
            <Delete id={course.id} />
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  const renderItem = ({item}) => <Item course={item} />

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.headerTitle}>CoderHouse</Text>
      </View>

      <View style={styles.sectionContainer}>
        <FlatList
          data={courses}
          renderItem={renderItem}
          keyExtractor={() => (Math.random() * 100).toString()}
          onEndReached={loadMoreCourses}
          onEndReachedThreshold={0.5}
          ListFooterComponent={() => (
            <View style={styles.activityIndicator}>
              <ActivityIndicator size={25} color="#5856d6" />
            </View>
          )}
        />
        {/* <TouchableOpacity style={styles.fabLocationBR} onPress={() => setModalVisible(true)}>
          <View style={styles.fab}>
            <Text style={styles.fabText}>+</Text>
          </View>
        </TouchableOpacity> */}
      </View>

      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => setModalVisible(!modalVisible)}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Agregar un curso</Text>
            <TextInput
              placeholder="Titulo"
              value={title}
              onChangeText={text => handleChangeText(text, 'title')}
            />
            <TextInput
              multiline={true}
              value={description}
              numberOfLines={2}
              placeholder="Descripcion"
              onChangeText={text => handleChangeText(text, 'description')}
            />
            <View style={styles.modalActions}>
              <Button title="Guardar" onPress={handleItemSave} />
              <Pressable onPress={() => setModalVisible(false)}>
                <Text style={styles.actionDelete}>Cancelar</Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  )
}

export default CoursesScreen

import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    flexDirection: 'row',
    backgroundColor: 'black',
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerTitle: {
    color: '#fff',
    fontSize: 22,
    fontWeight: 'bold',
  },
  sectionContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 20,
    paddingRight: 20,
  },
  cardContainer: {
    flex: 1,
    padding: 20,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: 'thistle',
    margin: 10,
  },
  titleCard: {
    alignSelf: 'center',
    fontSize: 16,
    marginBottom: 10,
    fontWeight: 'bold',
  },
  actionsContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  badge: {
    padding: 4,
    margin: 10,
    marginRight: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
  },
  badgeFavorite: {
    backgroundColor: 'black',
  },
  badgeDelete: {
    backgroundColor: 'red',
  },
  badgeText: {
    color: '#fff',
  },
  badgeTextDelete: {
    color: 'red',
  },
  fabLocationBR: {
    position: 'absolute',
    bottom: 25,
    right: 25,
  },
  fab: {
    backgroundColor: '#5856D6',
    width: 60,
    height: 60,
    borderRadius: 100,
    justifyContent: 'center',
  },
  fabText: {
    color: 'white',
    fontSize: 25,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    width: '80%',
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  modalActions: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    alignItems: 'center',
  },
  actionDelete: {
    color: 'red',
    marginLeft: 10,
  },
  modalText: {
    marginBottom: 15,
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
  },
  activityIndicator: {
    height: 150,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
})

export default styles

import {StyleSheet} from 'react-native'

export const style = StyleSheet.create({
  containerFlex: {
    flex: 1,
  },
  headerContainer: {
    marginVertical: 20,
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  signupContainer: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  formContainer: {
    width: '90%',
  },
  inputText: {
    marginVertical: 10,
    marginHorizontal: 5,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: 'rgba(0, 0, 0, 0.4)',
  },
  footerContainer: {
    width: '80%',
    marginVertical: 40,
  },
  ctaTycContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tycText: {
    fontWeight: 'bold',
  },
})

import React from 'react'
import {
  Platform,
  TextInput,
  View,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  Text,
  ScrollView,
  TouchableOpacity,
} from 'react-native'
import {CustomButton} from '../../components/CustomButton'
import {style} from './style'

const SignUpScreen = () => {
  return (
    <KeyboardAvoidingView
      style={style.containerFlex}
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      <ScrollView>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={style.signupContainer}>
            <View style={style.headerContainer}>
              <Text style={style.headerText}>Completa tus datos:</Text>
            </View>
            <View style={style.formContainer}>
              <TextInput
                style={style.inputText}
                placeholder="Nombre"
                autoCorrect={false}
                keyboardType="name-phone-pad"
                autoCapitalize="words"
              />
              <TextInput
                style={style.inputText}
                placeholder="Apellido"
                autoCorrect={false}
                keyboardType="name-phone-pad"
                autoCapitalize="words"
              />
              <View style={{flexDirection: 'row'}}>
                <TextInput
                  style={{...style.inputText, width: '20%'}}
                  placeholder="Cod. Area"
                  keyboardType="number-pad"
                />
                <TextInput
                  style={{...style.inputText, width: '75%'}}
                  placeholder="Nro"
                  keyboardType="number-pad"
                />
              </View>
              <TextInput
                style={style.inputText}
                placeholder="Email"
                autoCorrect={false}
                autoCapitalize="none"
                keyboardType="email-address"
              />
              <TextInput
                style={style.inputText}
                placeholder="Clave"
                autoCorrect={false}
                autoCapitalize="none"
                secureTextEntry
              />
              <TextInput
                style={style.inputText}
                placeholder="Repetir clave"
                autoCorrect={false}
                autoCapitalize="none"
                secureTextEntry
              />
            </View>
            <View style={style.footerContainer}>
              <CustomButton />
              <View style={style.ctaTycContainer}>
                <Text>Al continuar aceptas los </Text>
                <TouchableOpacity>
                  <Text style={style.tycText}>Terminos y Condiciones</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </ScrollView>
    </KeyboardAvoidingView>
  )
}

export default SignUpScreen

import React from 'react'
import CardCourse from '../../components/course/CardCourse'

const DetailsCourseScreen = ({route}) => {
  const {id} = route.params

  return <CardCourse id={id} />
}

export default DetailsCourseScreen

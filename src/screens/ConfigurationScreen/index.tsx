import React, {useState} from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {CustomSwitch} from '../../components/CustomSwitch'

const ConfigurationScreen = () => {
  const [state, setState] = useState({
    activeDarkMode: false,
    activeNotifications: false,
  })

  const {activeDarkMode, activeNotifications} = state

  const onChange = (value: boolean, field: string) => {
    setState({
      ...state,
      [field]: value,
    })
  }

  return (
    <View style={styles.configurationContainer}>
      <View>
        <View style={styles.itemContainer}>
          <View style={styles.item}>
            <Text>Dark Mode</Text>
            <CustomSwitch
              isActive={activeDarkMode}
              onChange={value => onChange(value, 'activeDarkMode')}
            />
          </View>
        </View>
        <View style={styles.itemContainer}>
          <View style={styles.item}>
            <Text>Notificaciones</Text>
            <CustomSwitch
              isActive={activeNotifications}
              onChange={value => onChange(value, 'activeNotifications')}
            />
          </View>
        </View>
      </View>
      <View style={styles.footerContainer}>
        <Text style={styles.footerText}>Coderhouse v0.0.1</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  configurationContainer: {
    flex: 1,
  },
  itemContainer: {
    borderBottomWidth: 0.5,
    borderColor: 'black',
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 40,
    marginLeft: 30,
    marginRight: 30,
    marginBottom: 8,
  },
  footerContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginBottom: 20,
  },
  footerText: {
    fontWeight: 'bold',
    fontSize: 14,
  },
})

export default ConfigurationScreen

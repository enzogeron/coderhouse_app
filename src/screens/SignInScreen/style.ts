import {StyleSheet} from 'react-native'

export const style = StyleSheet.create({
  containerFlex: {
    flex: 1,
  },
  signinContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  headerContainer: {
    marginHorizontal: 10,
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  formContainer: {
    width: '90%',
  },
  inputText: {
    marginVertical: 10,
    marginHorizontal: 5,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: 'rgba(0, 0, 0, 0.4)',
  },
  footerContainer: {
    width: '80%',
  },
  ctaSigninContainer: {
    textAlign: 'center',
    margin: 10,
  },
  signinButton: {
    width: '100%',
    height: 40,
    backgroundColor: '#000',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
  },
  signinText: {
    color: '#fff',
    fontWeight: 'bold',
  },
  ctaSignupContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  signupText: {
    fontWeight: 'bold',
  },
})

import React from 'react'
import {StackScreenProps} from '@react-navigation/stack'
import {
  Text,
  View,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
  TouchableOpacity,
} from 'react-native'
import {useForm} from '../../hooks/useForm'
import {style} from './style'
import {CustomButton} from '../../components/CustomButton/index'

interface Props extends StackScreenProps<any, any> {}

const SignInScreen = ({navigation}: Props) => {
  const {handleChangeText} = useForm({
    email: '',
    password: '',
  })

  return (
    <KeyboardAvoidingView
      style={style.containerFlex}
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={style.signinContainer}>
          <View style={style.headerContainer}>
            <Text style={style.headerText}>Hola! estas listo para comenzar.</Text>
            <Text style={style.headerText}>Te estamos esperando</Text>
          </View>
          <View style={style.formContainer}>
            <TextInput
              style={style.inputText}
              placeholder="Email"
              autoCorrect={false}
              autoCapitalize="none"
              keyboardType="email-address"
              onChangeText={value => handleChangeText(value, 'email')}
            />
            <TextInput
              style={style.inputText}
              placeholder="Clave"
              autoCorrect={false}
              autoCapitalize="none"
              secureTextEntry
              onChangeText={value => handleChangeText(value, 'password')}
            />
          </View>
          <View style={style.footerContainer}>
            <View style={style.ctaSigninContainer}>
              <CustomButton />
            </View>
            <View style={style.ctaSignupContainer}>
              <Text>Todavia no tenes cuenta? </Text>
              <TouchableOpacity onPress={() => navigation.navigate('Registrarse')}>
                <Text style={style.signupText}>Registrate</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  )
}

export default SignInScreen

export interface CourseInterface {
  id: number
  title: String
  description: String
  favorite: boolean
  price: number
  discount: number
  duration: String
  type: String
  category: String
}

export interface CoursesState {
  courses: CourseInterface[]
  loading: Boolean
  error: String | null
}

export const GET_COURSES_FETCH = 'GET_COURSES_FETCH'
export const GET_COURSES = 'GET_COURSES'
export const GET_COURSES_FAILURE = 'GET_COURSES_FAILURE'

export const UPDATE_COURSES = 'UPDATE_COURSES'

interface GetCoursesAction {
  type: typeof GET_COURSES
  payload: CourseInterface[]
}

interface GetCoursesFetchAction {
  type: typeof GET_COURSES_FETCH
}

interface GetCoursesFailureAction {
  type: typeof GET_COURSES_FAILURE
  payload: any
}
interface UpdateCoursesAction {
  type: typeof UPDATE_COURSES
  payload: CourseInterface[]
}

export type CoursesActionTypes =
  | GetCoursesFetchAction
  | GetCoursesAction
  | GetCoursesFailureAction
  | UpdateCoursesAction

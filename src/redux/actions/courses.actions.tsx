import {ActionCreator} from 'redux'
import {
  GET_COURSES,
  GET_COURSES_FETCH,
  GET_COURSES_FAILURE,
  CoursesActionTypes,
  CourseInterface,
} from '../types'
import {coursesService} from '../../services/courses.services'

const getCoursesFetch: ActionCreator<CoursesActionTypes> = () => {
  return {
    type: GET_COURSES_FETCH,
  }
}

const getCoursesSuccess: ActionCreator<CoursesActionTypes> = (courses: CourseInterface[]) => {
  return {
    type: GET_COURSES,
    payload: courses,
  }
}

const getCoursesFailure: ActionCreator<CoursesActionTypes> = (error: String) => {
  return {
    type: GET_COURSES_FAILURE,
    payload: error,
  }
}

export function getCourses() {
  return async dispatch => {
    try {
      dispatch(getCoursesFetch())
      const courses = await coursesService.getCourses()
      dispatch(getCoursesSuccess(courses))
    } catch (err) {
      dispatch(getCoursesFailure(err))
    }
  }
}

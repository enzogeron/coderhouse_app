import {
  CoursesState,
  GET_COURSES_FETCH,
  GET_COURSES,
  GET_COURSES_FAILURE,
  CoursesActionTypes,
} from '../types'

const initialState: CoursesState = {
  courses: [],
  loading: false,
  error: null,
}

export function coursesReducer(
  state: CoursesState = initialState,
  action: CoursesActionTypes,
): CoursesState {
  switch (action.type) {
    case GET_COURSES_FETCH: {
      return {
        ...state,
        loading: true,
      }
    }
    case GET_COURSES: {
      return {
        ...state,
        courses: action.payload,
        loading: false,
        error: null,
      }
    }
    case GET_COURSES_FAILURE: {
      return {
        ...state,
        loading: false,
        error: action.payload,
      }
    }
    default:
      return state
  }
}

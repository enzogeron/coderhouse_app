import {mockDataCourses} from '../data'
import {CourseInterface} from '../redux/types'

async function getCourses(): Promise<CourseInterface[]> {
  return mockDataCourses
}

export const coursesService = {getCourses}

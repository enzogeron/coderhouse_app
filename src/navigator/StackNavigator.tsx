import React from 'react'
import {createStackNavigator} from '@react-navigation/stack'
import CoursesScreen from '../screens/CoursesScreen'
import DetailsCourseScreen from '../screens/DetailsCourseScreen'

export type RockStackParams = {
  Courses: undefined
  DetailsCourse: undefined
}

const Stack = createStackNavigator<RockStackParams>()

export const StackNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="Courses"
      screenOptions={{
        headerStyle: {
          elevation: 0,
          shadowColor: 'transparent',
        },
        cardStyle: {
          backgroundColor: 'white',
        },
      }}>
      <Stack.Screen
        name="Courses"
        options={{
          headerShown: false,
        }}
        component={CoursesScreen}
      />
      <Stack.Screen
        name="DetailsCourse"
        options={{headerTitle: ''}}
        component={DetailsCourseScreen}
      />
    </Stack.Navigator>
  )
}

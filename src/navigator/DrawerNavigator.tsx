import React from 'react'
import {createDrawerNavigator} from '@react-navigation/drawer'
import {StackNavigator} from './StackNavigator'
import ConfigurationScreen from '../screens/ConfigurationScreen'
import SignInScreen from '../screens/SignInScreen'
import SignUpScreen from '../screens/SignUpScreen'

const Drawer = createDrawerNavigator()

export const DrawerNavigator = () => {
  return (
    <Drawer.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Drawer.Screen name="Cursos" component={StackNavigator} />
      <Drawer.Screen
        options={{headerShown: true}}
        name="Configuracion"
        component={ConfigurationScreen}
      />
      <Drawer.Screen name="Ingresar" component={SignInScreen} />
      <Drawer.Screen name="Registrarse" component={SignUpScreen} />
    </Drawer.Navigator>
  )
}

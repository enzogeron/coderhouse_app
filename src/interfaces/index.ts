export interface Course {
  id: number
  title: String
  description: String
  favorite: boolean
  price: number
  discount: number
  duration: String
  type: String
  category: String
}

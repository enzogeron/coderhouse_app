import {Course} from '../interfaces'

export const mockDataCourses: Course[] = [
  {
    id: 1001,
    title: 'Desarrollo de Aplicaciones',
    description:
      'En este curso aprenderás los conocimientos y bases para crear aplicaciones bridge con capacidad de despliegue tanto para Android como iOS, utilizando React Native, Javascript y un software de prototipado como Sketch o Adobe Xd.',
    favorite: false,
    price: 12000,
    discount: 40,
    duration: '12 semanas',
    type: 'Curso',
    category: 'Programacion',
  },
  {
    id: 1002,
    title: 'Diseño UX/UI',
    description:
      'Aprenderás a analizar y mejorar todas las aristas que involucran la Experiencia de Usuario de productos digitales. Crearás e iterarás tu proyecto desde una concepción MVP enfocándote en tu usuario modelo.',
    favorite: false,
    price: 24000,
    discount: 60,
    duration: '30 semanas',
    type: 'Carrera',
    category: 'Diseño',
  },
  {
    id: 1003,
    title: 'JavaScript',
    description:
      'En este curso aprenderás los fundamentos del lenguaje de programación más usado en la actualidad, con el cual es posible crear aplicaciones de todo tipo. Explorarás inicialmente herramientas propias del mismo, indagando casos prácticos de aplicación.',
    favorite: false,
    price: 18000,
    discount: 15,
    duration: '15 semanas',
    type: 'Curso',
    category: 'Programacion',
  },
  {
    id: 1004,
    title: 'React JS',
    description:
      'En este curso, que es el tercer nivel de la carrera de programación, aprenderás a programar por componentes, mediante Javascript, JS, ES6, y también conocerás las ventajas de la utilización del flujos de datos.',
    favorite: false,
    price: 14000,
    discount: 15,
    duration: '15 semanas',
    type: 'Curso',
    category: 'Programacion',
  },
  {
    id: 1005,
    title: 'Data Scientist',
    description:
      'En esta carrera aprenderás los conceptos generales de las bases de datos para luego trabajar sobre bases de datos relacionales.',
    favorite: false,
    price: 18000,
    discount: 15,
    duration: '15 semanas',
    type: 'Curso',
    category: 'Data',
  },
  {
    id: 1006,
    title: 'Data Analytics',
    description:
      'En este curso aprenderás los conceptos generales de las bases de datos para luego trabajar sobre bases de datos relacionales.',
    favorite: false,
    price: 18000,
    discount: 30,
    duration: '14 semanas',
    type: 'Carrera',
    category: 'Data',
  },
  {
    id: 1007,
    title: 'SQL',
    description:
      'En este curso aprenderás las nociones centrales de las bases de datos relacionales, las cuales son implementadas por todas las organizaciones para poder tomar decisiones con base en la información que generan en su modelo de negocio.',
    favorite: false,
    price: 22000,
    discount: 10,
    duration: '18 semanas',
    type: 'Curso',
    category: 'Programacion',
  },
]

import React from 'react'
import {Text, TouchableOpacity, View, StyleSheet} from 'react-native'

export const CustomButton = () => {
  return (
    <View style={style.ctaSigninContainer}>
      <TouchableOpacity>
        <View style={style.signinButton}>
          <Text style={style.signinText}>Ingresar</Text>
        </View>
      </TouchableOpacity>
    </View>
  )
}

const style = StyleSheet.create({
  ctaSigninContainer: {
    textAlign: 'center',
    margin: 10,
  },
  signinButton: {
    width: '100%',
    height: 40,
    backgroundColor: '#000',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
  },
  signinText: {
    color: '#fff',
    fontWeight: 'bold',
  },
})

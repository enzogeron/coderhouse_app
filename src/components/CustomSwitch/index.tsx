import React, {useState} from 'react'
import {Platform, Switch} from 'react-native'

interface Props {
  isActive: boolean
  onChange: (value: boolean) => void
}

export const CustomSwitch = ({isActive, onChange}: Props) => {
  const [active, setActive] = useState(isActive)

  const toggleSwitch = () => {
    setActive(!active)
    onChange(!active)
  }

  return (
    <Switch
      trackColor={{false: '#D9D9DB', true: '#5856D6'}}
      thumbColor={Platform.OS === 'android' ? '#5856D6' : ''}
      onValueChange={toggleSwitch}
      value={active}
    />
  )
}

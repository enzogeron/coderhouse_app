import React, {useEffect, useState} from 'react'
import {View, Text, StyleSheet, Button} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import {mockDataCourses} from '../../data'
import {Course} from '../../interfaces'

const CardCourse = ({id}) => {
  const [course, setCourse] = useState<Course>()

  useEffect(() => {
    const findCourse = mockDataCourses.find(x => x.id === id)
    setCourse(findCourse)
  }, [id])

  return (
    <View style={styles.cardContainer}>
      <Text style={styles.badge}>{`${course?.discount}% OFF`}</Text>
      <View style={styles.detailsContainer}>
        <Text style={styles.title}>{course?.title}</Text>
        <Text style={styles.hint}>
          {course?.type} - {course?.category}
        </Text>
        <Text style={styles.description}>{course?.description}</Text>
        <Text>
          <Icon name="cash-outline" size={20} color="black" />
        </Text>
        <Text>{`$${course?.price}`}</Text>
      </View>
      <View style={styles.cta}>
        <Button
          color={'black'}
          title="Inscribirme"
          onPress={() => console.log('Inscribirme al curso')}
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  cardContainer: {
    flex: 1,
    alignItems: 'center',
    marginLeft: 20,
    marginRight: 20,
  },
  detailsContainer: {
    textAlign: 'center',
    alignItems: 'center',
    borderRadius: 50,
    borderWidth: 1,
    borderColor: 'black',
    padding: 20,
  },
  badge: {
    position: 'absolute',
    zIndex: 1,
    backgroundColor: 'black',
    fontWeight: 'bold',
    color: '#fff',
    padding: 6,
    borderRadius: 50,
    alignSelf: 'flex-end',
    top: -10,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  hint: {
    color: '#2f2f2f',
    fontStyle: 'italic',
    marginBottom: 20,
  },
  description: {
    lineHeight: 20,
    textAlign: 'justify',
    marginBottom: 20,
  },
  cta: {
    margin: 20,
  },
})

export default CardCourse

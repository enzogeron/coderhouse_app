module.exports = {
  root: true,
  extends: ['@react-native-community'],
  parserOptions: {
    project: './tsconfig.json',
  },
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': 'error',
    semi: 0,
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
}
